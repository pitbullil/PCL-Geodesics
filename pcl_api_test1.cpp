/* read mesh from file and 
 - if one vertex is specified, for all vertices of the mesh print their distances to this vertex
 - if two vertices are specified, print the shortest path between these vertices 

	Danil Kirsanov, 01/2008 
*/
#include <iostream>
#include <fstream>
#include <string>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/PolygonMesh.h>
#include <pcl/common/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <geodesic_pcl_api.h>
#include <cloud_visualization_visl.hpp>

using namespace pcl;
using namespace std;

int main(int argc, char **argv) 
{
	if(argc < 2)
	{
		std::cout << "usage: mesh_file_name " << std::endl; //try: "hedgehog_mesh.txt 3 14" or "flat_triangular_mesh.txt 1"
		return 0;
	}

	string meshin = argv[1];
	PolygonMeshPtr mesh(new PolygonMesh());
	io::loadPLYFile(meshin, *mesh);

	Geo_Estimator *geoest;
	unsigned int subdiv = 2;
	geoest = new Geo_Subdiv_Estimator(subdiv);
	geoest->initialize_mesh(mesh);
	geoest->init_alg();
	geoest->find_focal();
	PointCloud<PointXYZI>::Ptr cloud = geoest->distance_cloud();
	double norm = geoest->GetMaxDistance();
	for (int i = 0; i < cloud->size(); i++) {
		cloud->at(i).intensity *= (255 / norm);
	}
	PointCloud<PointXYZRGB>::Ptr color = visualizeCloud<PointXYZI>(cloud);
	io::savePLYFile("geosubdiv.ply", *color);
	PointCloud<PointXYZI>::Ptr radius = geoest->get_path(geoest->GetIndex());
	for (int i = 0; i < radius->size(); i++) {
		radius->at(i).intensity *= (255 / norm);
	}
	PointCloud<PointXYZRGB>::Ptr colorpath = visualizeCloud<PointXYZI>(radius);
	io::savePLYFile("geosubdivR.ply", *colorpath);

	geoest = new MMP_Estimator<geodesic::GeodesicAlgorithmExact>();
	geoest->initialize_mesh(mesh);
	geoest->init_alg();
	geoest->find_focal();
	cloud = geoest->distance_cloud();
	norm = geoest->GetMaxDistance();
	for (int i = 0; i < cloud->size(); i++) {
		cloud->at(i).intensity *= (255 / norm);
	}
	color = visualizeCloud<PointXYZI>(cloud);
	io::savePLYFile("geoexact.ply", *color);
	radius = geoest->get_path(geoest->GetIndex());
	for (int i = 0; i < radius->size(); i++) {
		radius->at(i).intensity *= (255 / norm);
	}
	colorpath = visualizeCloud<PointXYZI>(radius);
	io::savePLYFile("geoexactR.ply", *colorpath);
	return 0;
}	