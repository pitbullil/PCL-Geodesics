#pragma once
#ifndef PROJ_NAMESPACES_H
#define PROJ_NAMESPACES_H
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <string>
#include <algorithm>
#include <queue>
#include <cstdint>
using namespace std;

#define MAXXXX 0xffffffff
namespace dis3{

	enum nnfDest   {T,Q,TKEY,QKEY,R,RKEY,RNNF,TFEATURE,QFEATURE,NNF,NNF1,NNF5,NNF1COLOR,NNF5COLOR,NNFCOLOR,SIMILARITY,SIMILARITYCOLOR,RESULT,TSURF,QSURF,KAPPA,DIST,CLOSEST, RMESH,RBOUNDARY,TBOUNDARY};
	enum patchMode {GEODESIC,GEONN,SPHERE,Geocut};
	enum FeatureRadiusMode {DIRECT,FRAC,FRACQ,FRACT};
	enum SurfReconMethod {POISSON,FTRIANGLE,NONE,LOAD};
	enum SimilarityMeasure {DIS,WDIS,DDIS,WBBS,BBS,NDIS};
	enum DistanceFunction {CHI2,L2F,L1F};
	enum KeypointTypes {ISS,HARRIS,SIFT,NARF,GRID,BOUND,ALL};
	enum MeshType {DUAL,NORMAL,SUPER};
	const float RADIUS_SEARCH_MULTIPLIER = 3;
	const float FPFH_SEARCH_MULTIPLIER = 1.6;
	typedef pcl::PointXYZ XYZ;
	typedef pcl::PointCloud<XYZ> XYZCloud;
	typedef pcl::PointXYZL XYZL;
	typedef pcl::PointCloud<XYZL> XYZLCloud;
	typedef pcl::PointXYZI XYZI;
	typedef pcl::PointCloud<XYZI> XYZICloud;
	typedef pcl::Normal N;
	typedef pcl::PointCloud<N> NormalCloud;
	typedef pcl::PFHSignature125 PFH;	
	typedef pcl::PointCloud<PFH> PFHCloud;
	typedef pcl::FPFHSignature33 FPFH;
	typedef pcl::PointCloud<FPFH> FPFHCloud;
	typedef pcl::SHOT352 SHOT;
	typedef pcl::PointCloud<SHOT> SHOTCloud;
	typedef pcl::PointCloud<pcl::PointXYZRGBNormal> XYZRGBNCloud;
	typedef pcl::PointCloud<pcl::PointXYZRGB> XYZRGBCloud;
	typedef pcl::PointCloud<pcl::PointNormal> XYZNCloud;

	struct barycentric_polygon_s {
		int index;
		float coor[3];
	};

	struct cloud_statistics_s {
		float MeanResolution, Range, MaxResolution;
	};

	struct surface_recon_params_s {
		MeshType mt = SUPER;
		SurfReconMethod Method = LOAD;
		std::string method = "LOAD";
		float searchRadius, Mu, upsamplingR, upsamplingStep, cloudResolution;
		int NN = 100, depth = 9, clean_multiplier = 1;
		bool base = true;
		bool clean = true;
		bool save = true;
		bool grid = false;
		bool normals = true;
		std::string out_path, in_path;
	};

	struct matches_stat_s {
		float exact=0, lessone=0, lesstwo=0;
	};

	struct distance_stat {
		float mu=0;
		float sigma=0;
		float exact = 0;
		float lessE = 0;
		float less2E = 0;
		float total_inrange = 0;
	};

	struct keypoint_params_s {
		double modelResolution=0;
		float salient_radius=0;
		float non_max_radius=0;
		float gamma_21 = 0.975;
		float gamma_32 = 0.975;
		int min_neighbors = 10;
		KeypointTypes Method=GRID;
		int threads;
		bool border = false;
		bool save = false;
		bool autodefault = true;
		std::string path;
	};

	struct feature_params_s {
		float searchRadius=0;
		std::string path;
	};
	
	struct vert_dis_s {
		int ind;
		float dis = 0;
	};

	class vertdisCompare {
	public:
		bool operator()(const vert_dis_s &a, const vert_dis_s & b) {
			return (a.dis < b.dis);
		};
	};

	typedef priority_queue<vert_dis_s, vector<vert_dis_s>, vertdisCompare> DISQ;

	struct cloud_param_s {
		feature_params_s f;
		keypoint_params_s k;
		surface_recon_params_s s;
		cloud_statistics_s stat;
		std::string path, in;
		float diam, area,max_edge,mean_edge;
		float RG;//DEBUG FEATURES
		
	};

	struct template_match_parms_s {
		float R_E, R_G, frac;
		patchMode pMode=GEODESIC;
		uint32_t focal_ind = MAXXXX;
		float nnf_reject_threshold = 1;
		SimilarityMeasure similarity=DIS;
		SurfReconMethod surface_method = LOAD;
		MeshType meshtype = SUPER;
		KeypointTypes keytype = ALL;
		std::string Rdir, feature = "FPFH", keypoint="ALL", radius="0", surface="", sim="DIS", patch="GEODESIC",mesh="SUPER",nnf_dir,result_dir,fractype,MDS;
		bool normal_by_nn=false;
		bool mesh_normals = false;
		bool normal_features = false;
		cloud_param_s T, S;
		nnfDest nnfType = NNF1;
		bool do_ransac;
		//DEBUG Parameters
		bool debug;
		uint32_t focal_gt, focal_gt_sym;
		FeatureRadiusMode RMode =  DIRECT;
	};



}

#endif