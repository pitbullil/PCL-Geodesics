#include <misc_utils.hpp>
using namespace std;

float stringetof(string in) {
	return atof(in.c_str());
}

int stringetoi(string in) {
	return atoi(in.c_str());
}

int barycentric_closest(barycentric_polygon_s bar, pcl::Vertices poly) {
	int i = 0;
	float max = bar.coor[0];
	if (bar.coor[1] > max) {
		i = 1;
		max = bar.coor[1];
	}
	if (bar.coor[2] > max) {
		i = 2;
	}
	return poly.vertices[i];
};

string ftos(float f, int nd=2) {
	std::ostringstream buffer;
	
	buffer << std::fixed << std::setprecision(nd) << f;
	return buffer.str();
}

void parse_Rmode(string in, template_match_parms_s& p) {
	p.fractype = in;
	if (in.compare(0, 6, "DIRECT") == 0) {
		p.RMode = DIRECT;
	}
	else if (in.compare(0, 5, "FRACQ") == 0) {
		p.RMode = FRACQ;
	}
	else if (in.compare(0, 5, "FRACT") == 0) {
		p.RMode = FRACT;
	}
	else if (in.compare(0, 4, "FRAC") == 0) {
		p.RMode = FRAC;
	}
}

void parse_feature(string in, template_match_parms_s& p) {
	p.feature = in;
}

void parse_surface(string in, template_match_parms_s& p) {
	p.surface = in;
	string surface_dir = in;
	if (in.compare(0, 7, "POISSON") == 0) { p.T.s.Method = POISSON; p.S.s.Method = POISSON; p.surface_method = POISSON;
	}
	else if (in.compare(0, 8, "TRIANGLE") == 0) { 
		p.T.s.Method = FTRIANGLE; p.S.s.Method = FTRIANGLE; p.surface_method = FTRIANGLE;
	}
	else if (in.compare(0, 4, "LOAD") == 0) { 
		p.T.s.Method = LOAD; p.S.s.Method = LOAD; p.surface_method = LOAD;
	}
	else { p.T.s.Method = NONE; p.S.s.Method = NONE;  p.surface_method = NONE;
	}
	p.T.s.method = in; p.S.s.method = in;
}

void parse_keypoint(string in, template_match_parms_s& p) {
	p.keypoint = in;
	if (in.compare(0, 3, "ISS") == 0) {
		p.T.k.Method = ISS; p.S.k.Method = ISS;
		//p.modelResolution = 3 * T_s.MeanResolution;
	}
	else if (in.compare(0, 4, "GRID") == 0) {
		p.T.k.Method = GRID; p.S.k.Method = GRID;
	}
	else if (in.compare(0, 3, "ALL") == 0) {
		p.T.k.Method = ALL; p.S.k.Method = ALL;
	}
	else if (in.compare(0, 6, "HARRIS") == 0) {
		p.T.k.Method = HARRIS; p.S.k.Method = HARRIS;
	}
	else if (in.compare(0, 4, "SIFT") == 0) {
		p.T.k.Method = SIFT; p.S.k.Method = SIFT;
	}
	else if (in.compare(0, 5, "BOUND") == 0) {
		p.T.k.Method = BOUND; p.S.k.Method = BOUND;
		//p_t.T.k.salient_radius = feature_t_radius;
		//p_t.S.k.salient_radius = feature_o_radius;
	}
}

void parse_template(string in, template_match_parms_s& p) {
	p.patch = in;
	if (in.compare(0, 6, "SPHERE") == 0) {
		p.pMode = SPHERE;
	}
	else if (in.compare(0, 8, "GEODESIC") == 0) {
		p.pMode = GEODESIC;
	}
	else if (in.compare(0, 5, "GEONN") == 0) {
		p.pMode = GEONN;
	}
}

void parse_similarity(string in, template_match_parms_s& p) {
	p.sim = in;
	if (in.compare(0, 3, "DIS") == 0) {
		p.similarity = DIS; 
	}
	else if (in.compare(0, 4, "DDIS") == 0) {
		p.similarity = DDIS;
	}
	else if (in.compare(0, 3, "BBS") == 0) {
		p.similarity = BBS;
	}
	else if (in.compare(0, 4, "WDIS") == 0) {
		p.similarity = WDIS;
	}
	else if (in.compare(0, 4, "WBBS") == 0) {
		p.similarity = WBBS;
	}
}

void parse_nnf(string in, template_match_parms_s& p) {
	int nn = stoi(in);
	switch (nn){
	case 0: p.nnfType = NNF; break;
	case 1: p.nnfType = NNF1; break;
	case 5: p.nnfType = NNF5; break;
	}
}

void parse_mesh(string in, template_match_parms_s& p) {
	p.mesh = in;
	if (in.compare(0, 5, "SUPER") == 0) {
		p.T.s.mt = SUPER;		p.S.s.mt = SUPER; p.meshtype = SUPER;
	}
	else if (in.compare(0, 4, "DUAL") == 0) {
		p.T.s.mt = DUAL;		p.S.s.mt = DUAL; p.meshtype = DUAL;
	}
	else if (in.compare(0, 6, "NORMAL") == 0) {
		p.T.s.mt = NORMAL;		p.S.s.mt = NORMAL; p.meshtype = NORMAL;
	}
}

void parse_r(string in, template_match_parms_s& p, nnfDest D) {
	feature_params_s& p_f = (D == Q) ? p.S.f : p.T.f;
	p_f.searchRadius = stof(in);
}

void parse_3dis_params(char** argv, int argc, template_match_parms_s& p) {
	p.T.in = argv[1]; p.S.in = argv[2];
	p.S.path = p.S.in.substr(0, p.S.in.size() - 4);
	p.T.path = p.T.in.substr(0, p.T.in.size() - 4);
	p.T.s.in_path = p.T.in; p.S.s.in_path = p.S.in;
	for (int i = 3; i < argc; i++) {
		string arg = argv[i];
		if (arg.compare(0, 8, "surface=") == 0){
			parse_surface(arg.substr(8), p);
		} else if (arg.compare(0, 9, "keypoint=" ) == 0) {
			parse_keypoint(arg.substr(9), p);
		} else if (arg.compare(0, 8, "feature=") == 0) {
			parse_feature(arg.substr(8),p);
		} else if (arg.compare(0, 9, "template=") == 0) {
			parse_template(arg.substr(9),p);
		} else if (arg.compare(0, 11, "similarity=") == 0) {
			parse_similarity(arg.substr(11), p);
		} else if (arg.compare(0, 5, "mesh=") == 0) {
			parse_mesh(arg.substr(5), p);
		} else if (arg.compare(0, 3, "RQ=") == 0) {
			parse_r(arg.substr(3), p,Q);
		} else if (arg.compare(0, 3, "RT=") == 0) {
			parse_r(arg.substr(3), p,T);
		}
		else if (arg.compare(0, 3, "NN=") == 0) {
			parse_nnf(arg.substr(3), p);
		}
		else if (arg.compare(0, 5, "NORMF") == 0) {
			p.normal_features = true;
		}
		else if (arg.compare(0, 6, "RMODE=") == 0) {
			parse_Rmode(arg.substr(6),p);
		}
		else if (arg.compare(0, 5, "FRAC=") == 0) {
			p.frac = stof(arg.substr(5));
		}
		else if (arg.compare(0, 9, "MESHNORMS") == 0) {
			p.mesh_normals = true;;
		}
		else if (arg.compare(0, 4, "MDS=") == 0) {
			p.MDS = arg.substr(4);
		}


	}
}

void create_output_directories(template_match_parms_s& p) {
	mkdir(p.T.path.c_str()); mkdir(p.S.path.c_str());
	p.Rdir = p.T.path + "\\" + p.S.path;
	mkdir(p.Rdir.c_str());
	string tmp_str = "";
	string surface_str = "";
	if ((p.surface_method != NONE) || (p.surface_method != LOAD)) {
		surface_str.append("\\"+p.surface);
		mkdir((p.T.path + "\\" + surface_str).c_str());
		mkdir((p.S.path + "\\" + surface_str).c_str());
		mkdir((p.Rdir + "\\" + surface_str).c_str());
		if (p.surface_method == POISSON) {
			surface_str.append("\\"+ to_string(p.T.s.depth));
			mkdir((p.T.path + "\\" + surface_str).c_str()); p.T.s.out_path = p.T.path + "\\" + surface_str + "\\S.ply";
			mkdir((p.S.path + "\\" + surface_str).c_str()); p.T.s.out_path = p.S.path + "\\" + surface_str + "\\S.ply";
			mkdir((p.Rdir + "\\" + surface_str).c_str());
		}
	}
	tmp_str.append(surface_str);
	tmp_str.append("\\" + p.keypoint); 
	p.T.k.path = p.T.path + tmp_str + "\\key.ply";
	p.S.k.path = p.S.path + tmp_str + "\\key.ply";
	mkdir((p.T.path + tmp_str).c_str());
	mkdir((p.S.path + tmp_str).c_str());
	mkdir((p.Rdir + tmp_str).c_str());
	tmp_str.append("\\"+p.feature);
	mkdir((p.T.path + tmp_str).c_str());
	mkdir((p.S.path + tmp_str).c_str());
	mkdir((p.Rdir + tmp_str).c_str());
	if (p.RMode == DIRECT) {
		p.T.f.path = p.T.path + tmp_str + "\\" + ftos(p.T.f.searchRadius, 3);
		p.S.f.path = p.S.path + tmp_str + "\\" + ftos(p.S.f.searchRadius, 3);
		mkdir(p.T.f.path.c_str());
		mkdir(p.S.f.path.c_str());
		tmp_str.append("\\" + ftos(p.T.f.searchRadius, 3));
		mkdir((p.Rdir + tmp_str).c_str());
		tmp_str.append("\\" + ftos(p.S.f.searchRadius, 3));
		mkdir((p.Rdir + tmp_str).c_str());
	}
	else {
		p.T.f.path = p.T.path + tmp_str + "\\" + p.fractype;
		p.S.f.path = p.S.path + tmp_str + "\\" + p.fractype;
		tmp_str.append("\\" + p.fractype);

		mkdir(p.T.f.path.c_str());
		mkdir(p.S.f.path.c_str());
		mkdir((p.Rdir + tmp_str).c_str());
		tmp_str.append("\\" + ftos(p.frac));

		p.T.f.path = p.T.path + tmp_str + "\\" ;
		p.S.f.path = p.S.path + tmp_str + "\\";
		mkdir(p.T.f.path.c_str());
		mkdir(p.S.f.path.c_str());
		mkdir((p.Rdir + tmp_str).c_str());

	}
	p.T.f.path.append("\\F.pcd");	p.S.f.path.append("\\F.pcd");
	
	p.nnf_dir = p.Rdir + tmp_str + "\\";
	if (p.normal_features) {
		p.nnf_dir.append("NORMF\\");
		mkdir(p.nnf_dir.c_str());
	}
	p.result_dir = p.nnf_dir + p.patch + "\\";
	mkdir(p.result_dir.c_str());
	if (p.pMode != SPHERE) {
		p.result_dir.append(p.mesh + "\\");
		mkdir(p.result_dir.c_str());
	}
	p.result_dir.append(p.sim + "\\");
	mkdir(p.result_dir.c_str());
}

string format_time_d_h_m_s(time_t elapsed) {

	double seconds = fmod(elapsed, 60);
	string result = ftos(seconds) + "\sS,\s";
	if (elapsed / 60 > 0) {
		int minutes = (int)floor(fmod(elapsed / 60, 60));
		result = to_string(minutes) + "\sM,\s" + result;
	}
	if (elapsed / 3600 > 0) {
		int	hours = (int)floor(fmod(elapsed / 3600, 24));
		result = to_string(hours) + "\sH,\s" + result;
	}
	if (elapsed / (24 * 3600) > 0) {
		int	days = (int)floor(elapsed / (24 * 3600));
		result = to_string(days) + "\sD,\s" + result;
	}
	return result;
}

string TimeLog::printlabel() {
	return label;
}

TimeLog::TimeLog(string label) : label(label) {
	time(&base);
	time(&elapsed);
	logged = false;
}

string TimeLog::LogToCSV() {
	return ftos(difftime(elapsed, base));
}

void TimeLog::SetTime() {
	time(&elapsed);
	logged = true;
}

string TimeLog::format_time_d_h_m_s() {
	float elapsed1 = difftime(elapsed, base);
	double seconds = fmod(elapsed1, 60);
	string result = ftos(seconds) + " S, ";
	if (elapsed1 / 60 > 0) {
		int minutes = (int)floor(fmod(elapsed1 / 60, 60));
		result = to_string(minutes) + " M, " + result;
	}
	if (elapsed1 / 3600 > 0) {
		int	hours = (int)floor(fmod(elapsed1 / 3600, 24));
		result = to_string(hours) + " H, " + result;
	}
	if (elapsed1 / (24 * 3600) > 0) {
		int	days = (int)floor(elapsed1 / (24 * 3600));
		result = to_string(days) + " D, " + result;
	}
	return result;
}

void TimeLog::Reset() {
	time(&base);
	time(&elapsed);
	logged = false;
};


void Time_LogtoCSV(std::vector<TimeLog*> logger, string out_path) {
	fstream stat_file(out_path, std::fstream::out);
	int size = logger.size();
	for (int i = 0; i < size -1; i++) {
		stat_file << logger[i]->printlabel()<<",";
	}
	stat_file << logger[size-1]->printlabel() << endl;
	for (int i = 0; i < size -1; i++) {
		stat_file << logger[i]->LogToCSV() << ",";
	}
	stat_file << logger[size - 1]->LogToCSV() << endl;

}

std::vector<LabeledStat> read_time_log(string filename) {
	std::vector<LabeledStat> result;
	ifstream file(filename);
	std::string labels;
	std::getline(file, labels);
	std::stringstream iss1(labels);
	std::string values;
	std::getline(file, values);
	std::stringstream iss2(values);

	while (true) {
		std::string label;
		std::getline(iss1, label, ',');
		LabeledStat stat;
		stat.Label = label;
		std::string value;
		std::getline(iss2, value, ',');
		stat.stat = stof(value);
		result.push_back(stat);
	}
}

//TODO
int load_center() {
	int center;
	return center;
}

string extract_filename(string s) {
	stringstream iss(s);
	std::string filename;

	while (iss.good())
	{
		getline(iss, filename, '\\');
		// Process SingleLine here
	}
	return filename;
}

bool check_num(std::string const &in) {
	char *end;

	strtol(in.c_str(), &end, 10);
	return !in.empty() && *end == '\0';
}

//////////////////////////////////////////////////////////////////////////////////////////////
void rand_sample_patch(vector<int>& indices, vector<float>& distances, float fraction) {
	vector<int> sample(indices.size());
	iota(sample.begin(), sample.end(), 0);
	random_shuffle(sample.begin(), sample.end());
	int delind = round(fraction*indices.size());
	sample.erase(sample.begin() + delind, sample.end());
	vector<int> temp_indices(sample.size());
	vector<float> temp_distances(sample.size());

	for (int i = 0; i < sample.size(); i++) {
		temp_indices.at(i) = indices.at(sample.at(i));
		temp_distances.at(i) = distances.at(sample.at(i));
	}
	indices = temp_indices;
	distances = temp_distances;
}

void
print4x4Matrix(const Eigen::Matrix4d & matrix)
{
	printf("Rotation matrix :\n");
	printf("    | %6.3f %6.3f %6.3f | \n", matrix(0, 0), matrix(0, 1), matrix(0, 2));
	printf("R = | %6.3f %6.3f %6.3f | \n", matrix(1, 0), matrix(1, 1), matrix(1, 2));
	printf("    | %6.3f %6.3f %6.3f | \n", matrix(2, 0), matrix(2, 1), matrix(2, 2));
	printf("Translation vector :\n");
	printf("t = < %6.3f, %6.3f, %6.3f >\n\n", matrix(0, 3), matrix(1, 3), matrix(2, 3));
}
