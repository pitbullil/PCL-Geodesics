cmake_minimum_required(VERSION 2.6 FATAL_ERROR)

project(geodesics)
find_package(PCL 1.8 REQUIRED)

set(MY_INCLUDE_DIR "./include")

include_directories(${PCL_INCLUDE_DIRS} ${MY_INCLUDE_DIR})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (pcl_api_test pcl_api_test.cpp cloud_visualization_visl.cpp misc_utils.cpp)
target_link_libraries (pcl_api_test ${PCL_LIBRARIES})

add_executable (pcl_api_test1 pcl_api_test1.cpp cloud_visualization_visl.cpp)
target_link_libraries (pcl_api_test1 ${PCL_LIBRARIES})

add_executable (example0 example0.cpp cloud_visualization_visl.cpp)
target_link_libraries (example0 ${PCL_LIBRARIES})

add_executable (example1 example1.cpp cloud_visualization_visl.cpp)
target_link_libraries (example1 ${PCL_LIBRARIES})