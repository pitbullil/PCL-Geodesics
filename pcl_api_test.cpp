/* read mesh from file and 
 - if one vertex is specified, for all vertices of the mesh print their distances to this vertex
 - if two vertices are specified, print the shortest path between these vertices 

	Danil Kirsanov, 01/2008 
*/
#include <iostream>
#include <fstream>
#include <string>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/PolygonMesh.h>
#include <pcl/common/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <geodesic_pcl_api.h>
#include <cloud_visualization_visl.hpp>
#include <time.h>
#include <misc_utils.hpp>


using namespace pcl;
using namespace std;

int main(int argc, char **argv) 
{
	if(argc < 2)
	{
		std::cout << "usage: mesh_file_name " << std::endl; //try: "hedgehog_mesh.txt 3 14" or "flat_triangular_mesh.txt 1"
		return 0;
	}
	time_t base_time;
	time(&base_time);
	std::vector<TimeLog*> logs;
	time_t timer;
	TimeLog exactT("Exact");
	TimeLog subDivT("SubDiv");

	string meshin = argv[1];
	PolygonMeshPtr mesh(new PolygonMesh());
	io::loadPLYFile(meshin, *mesh);
	time(&timer);
	subDivT.Reset();
	Geo_Estimator *geoest;
	unsigned int subdiv = 2;
	geoest = new Geo_Subdiv_Estimator(subdiv);
	geoest->initialize_mesh(mesh);
	geoest->init_alg();
	std::vector<std::vector<double>> D = geoest->CreateDistanceMatrix();
	save_distance_matrix("subdivD.csv",D);
	subDivT.SetTime();

	time(&timer);
	exactT.Reset();
	geoest = new MMP_Estimator<geodesic::GeodesicAlgorithmExact>();
	geoest->initialize_mesh(mesh);
	geoest->init_alg();
	D = geoest->CreateDistanceMatrix();
	save_distance_matrix("exactD.csv", D);
	exactT.SetTime();
	logs.push_back(&subDivT); logs.push_back(&exactT);
	Time_LogtoCSV(logs, "timelog.csv");
	return 0;
}	