#include <cloud_visualization_visl.hpp>

using namespace dis3;

bool color_point(const pcl::PointXYZI point_in, pcl::PointXYZRGB& point) {
	if (point_in.intensity == INFINITY) return false;
	point.r = hcmap[static_cast<int>(floor(point_in.intensity))][0];
	point.g = hcmap[static_cast<int>(floor(point_in.intensity))][1];
	point.b = hcmap[static_cast<int>(floor(point_in.intensity))][2];
	return true;
}

bool color_pointi(const pcl::PointXYZI point_in, pcl::PointXYZRGB& point) {
	if (point_in.intensity == INFINITY) return false;
	point.r = icmap[static_cast<int>(floor(point_in.intensity))][0];
	point.g = icmap[static_cast<int>(floor(point_in.intensity))][1];
	point.b = icmap[static_cast<int>(floor(point_in.intensity))][2];
	return true;

}

bool color_point(const pcl::PointXYZL point_in, pcl::PointXYZRGB& point) {
	point.r = lcmap[point_in.label % 1000][0];
	point.g = lcmap[point_in.label % 1000][1];
	point.b = lcmap[point_in.label % 1000][2];
	return true;
}

bool color_pointi(const pcl::PointXYZL point_in, pcl::PointXYZRGB& point) { return false; };

void visualize_keyPoints(pcl::PointCloud<pcl::PointXYZ>::Ptr keyPoints1,
	pcl::PointCloud<pcl::PointXYZ>::Ptr keyPoints2,
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointsCloud1,
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointsCloud2,
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointsCloud2_transformed,
	pcl::CorrespondencesPtr correpsondences,
	pcl::CorrespondencesPtr ransacCorrepsondences)
{
	pcl::visualization::PCLVisualizer viewer("PCL Viewer");

	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler1(keyPoints1, 0, 255, 0);
	viewer.addPointCloud(keyPoints1, keypoints_color_handler1, "keypoints1");
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler2(keyPoints2, 0, 255, 255);
	viewer.addPointCloud(keyPoints2, keypoints_color_handler2, "keypoints2");


	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_color_handler1(pointsCloud1, 0, 255, 0);
	viewer.addPointCloud(pointsCloud1, cloud_color_handler1, "cloud1");
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_color_handler2(pointsCloud2, 0, 255, 255);
	viewer.addPointCloud(pointsCloud2, cloud_color_handler2, "cloud2");
	double r, g, b;
	std::stringstream ss_line;
	// add matches before ransac:
	for (int i = 0; i < correpsondences->size(); i++) {
		ss_line << "correspondence_line" << i;
		viewer.addLine<pcl::PointXYZ, pcl::PointXYZ>(keyPoints1->points[correpsondences->at(i).index_query], keyPoints2->points[correpsondences->at(i).index_match], 255, 255, 0, ss_line.str());
	}
	// Add ransac matches:
	if (!ransacCorrepsondences->empty()) {
		for (int i = 0; i < ransacCorrepsondences->size(); i++) {
			ss_line << "ransac_correspondence_line" << i;
			r = 0;//double(rand() % 256) / 255;
			g = 255;//double(rand() % 256) / 255;
			b = 0;//double(rand() % 256) / 255;
			viewer.addLine<pcl::PointXYZ, pcl::PointXYZ>(keyPoints1->points[ransacCorrepsondences->at(i).index_query], keyPoints2->points[ransacCorrepsondences->at(i).index_match], r, g, b, ss_line.str());
		}
	}

	/*if (!pointsCloud2_transformed->empty()) {
		pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_color_handler3(pointsCloud2_transformed, 255, 255, 255);
		viewer.addPointCloud(pointsCloud2_transformed, cloud_color_handler2, "cloud2_transformed");
	}*/

	viewer.setBackgroundColor(0.0, 0.0, 0.0);
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints1");
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints2");
	while (!viewer.wasStopped())
	{
		viewer.spinOnce(100);
	}
}

/*void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event,
	void* viewer_void)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
	if (event.getKeySym() == "s" && event.keyDown())
	{
		std::cout << "s was pressed => saving snapshot" << std::endl;

		char str[512];
		for (unsigned int i = 0; i < text_id; ++i)
		{
			sprintf(str, "text#%03d", i);
			viewer->removeShape(str);
		}
		text_id = 0;
	}
}*/

